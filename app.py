from Protocol.app.block import Block
from Protocol.app.blockchain import Blockchain
from Protocol.app import *

b = Block.gen_genesis()

chain = Blockchain()

chain.add_block(b)

for i in chain.chain: print('\n\n', i, '\n\n')