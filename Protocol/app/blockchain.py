from Protocol.app import *
from Protocol.app.block import Block

class Blockchain:

    def __init__(self) -> None:
        self.chain: list[dict] = [Block.gen_genesis()]

    def add_block(self, data: str) -> dict:
        last_block = self.chain[-1]
        data = Block(int(now()), last_block['hash'], data, last_block['difficult'])
        adjust_difficult(data.block['timestamp'], int(now()))
        data.block['hash'] = Block.hash_block(data.block)
        self.chain.append(data.block)
        return data.block

    def validate_chain(self, chain: list):
        if chain[0] != Block.gen_genesis():
            return False
        else:
            # verificar os hash e last_hash 'batem'
            True

    def replace_chain(self, new_chain: list):
        if len(new_chain) > len(self.chain) and  self.validate_chain(new_chain):
            self.chain = new_chain
        else:
            return False

