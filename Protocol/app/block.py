from hashlib import sha256
from Protocol.app import *



class Block:
    def __init__(self, timestamp: int, last_hash: str, data: str, difficult: str) -> None:
        self.timestamp:int = timestamp
        self.last_hash:str = last_hash
        self.data: str = data
        self.difficult = difficult
        self.block: dict = {
            "timestamp": self.timestamp,
            "last_hash": self.last_hash,
            "data": self.data,
            "difficult": self.difficult
        }
        self.block["hash"] = self.hash_block(self.block)


    @staticmethod
    def gen_genesis() -> str:
        genesis_block: dict[str, int and str] = {}
        genesis_block['data'] = "Primeiro Block"
        genesis_block['timestamp'] = int(now())
        genesis_block['last_hash'] = "101"
        genesis_block['difficult'] = difficult
        genesis_block['hash'] = Block.hash_block(genesis_block)
        return genesis_block


    @staticmethod
    def hash_block(block: dict) -> str:
        nonce = 0
        hash = ''
        while not Block.validate_hash(hash):
            block['nonce'] = nonce
            hash = sha256(
                block.__str__().encode('utf-8')
            ).hexdigest()
            nonce += 1
        return hash

    @staticmethod
    def validate_hash(hash: str) -> bool:
        return hash.startswith("0" * difficult)

