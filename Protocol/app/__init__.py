from datetime import datetime

difficult: int = 1
mine_rate: int = 3000

now = datetime.now().timestamp

def adjust_difficult(last_block: int, current_time: int) -> None:
    
    global difficult
    if last_block + mine_rate > current_time:
        globals()['difficult'] = difficult + 1
    else:
        difficult -= difficult - 1
